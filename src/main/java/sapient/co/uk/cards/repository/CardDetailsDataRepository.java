package sapient.co.uk.cards.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import sapient.co.uk.cards.model.CardDetail;

@Repository
public interface CardDetailsDataRepository extends JpaRepository<CardDetail, Integer>{

}
