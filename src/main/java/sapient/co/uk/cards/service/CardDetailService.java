package sapient.co.uk.cards.service;

import java.io.IOException;
import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import sapient.co.uk.cards.model.CardDetail;
import sapient.co.uk.cards.repository.CardDetailsDataRepository;

@Service
public class CardDetailService {

	@Autowired
	private CardDetailsDataRepository repo;


	public  boolean validateCreditCardNumber(String str) {

		if(str.length()==19 && str.contains("-")) {
			str = str.replaceAll("-", "");
		}

		if (str.length()==16 && isNumeric(str) && luhnCreditCardNumber(str) ) {
			return true;

		}

		return false;
	}

	public  boolean luhnCreditCardNumber(String str) {

		int[] ints = new int[str.length()];
		for (int i = 0; i < str.length(); i++) {
			ints[i] = Integer.parseInt(str.substring(i, i + 1));
		}
		for (int i = ints.length - 2; i >= 0; i = i - 2) {
			int j = ints[i];
			j = j * 2;
			if (j > 9) {
				j = j % 10 + 1;
			}
			ints[i] = j;
		}
		int sum = 0;
		for (int i = 0; i < ints.length; i++) {
			sum += ints[i];
		}
		if (sum % 10 == 0) {
			System.out.println(str + " is a valid credit card number");
			return true;
		} else {
			System.out.println(str + " is an invalid credit card number");
			return false;
		}

	}

	public  boolean isNumeric(String string) {
		Long iValue;

		System.out.println(String.format("Parsing string: \"%s\"", string));

		if(string == null || string.equals("")) {
			System.out.println("String cannot be parsed, it is null or empty.");
			return false;
		}

		try {
			iValue = Long.parseLong(string);
			
			return true;
		} catch (NumberFormatException e) {
			System.out.println("Input String cannot be parsed to Integer.");
		}
		return false;
	}


}
