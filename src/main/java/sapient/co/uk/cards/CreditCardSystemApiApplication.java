package sapient.co.uk.cards;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreditCardSystemApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditCardSystemApiApplication.class, args);
	}

}
