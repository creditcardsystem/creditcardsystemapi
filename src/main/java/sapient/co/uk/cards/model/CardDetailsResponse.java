package sapient.co.uk.cards.model;

import sapient.co.uk.cards.model.CardDetail;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardDetailsResponse {

    private String status;
    private CardDetail cardDetail;
    private List<CardDetail> cardDetailList;
    private String errorMessage;

    public CardDetailsResponse(final CardDetail cardDetail) {
        this.cardDetail = cardDetail;
    }
    

    public CardDetailsResponse(final String errorMessage) {
        this.status = "error";
        this.errorMessage = errorMessage;
    }

    public CardDetailsResponse(List<CardDetail> cardDetailList) {
    	this.cardDetailList = cardDetailList;
	}


	@JsonGetter("status")
    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    @JsonGetter("card_detail")
    public CardDetail getCardDetail() {
        return cardDetail;
    }

    public void setCardDetail(final CardDetail cardDetail) {
        this.cardDetail = cardDetail;
    }

    @JsonGetter("error_message")
    public String getErrorMessage() {
        return errorMessage;
    }

    @JsonGetter("card_detail_list")
	public List<CardDetail> getCardDetailList() {
		return cardDetailList;
	}

	public void setCardDetailList(ArrayList<CardDetail> cardDetailList) {
		this.cardDetailList = cardDetailList;
	}
    
    

}
