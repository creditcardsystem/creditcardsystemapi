package sapient.co.uk.cards.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sapient.co.uk.cards.model.CardDetail;
import sapient.co.uk.cards.model.CardDetailsResponse;
import sapient.co.uk.cards.repository.CardDetailsDataRepository;
import sapient.co.uk.cards.service.CardDetailService;



@RestController
@CrossOrigin
public class CardController {
	@Autowired
	CardDetailsDataRepository cardDetailsDataRepository;
	
	@RequestMapping(value = "/ping", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
	public String ping() {
		return "pong";
	}
	
	@RequestMapping(value = "/card",produces = "application/json", method = RequestMethod.GET)
	public ResponseEntity<CardDetailsResponse>  getAllCards() {
		CardDetailsResponse cardDetailsResponse;
		cardDetailsResponse = new CardDetailsResponse(cardDetailsDataRepository.findAll());
   	 System.out.println("all - " + cardDetailsDataRepository.findAll().size() +"----"+cardDetailsResponse.getCardDetailList().size());
   			//cardDetailsDataRepository.findAll();
       return ResponseEntity.status(HttpStatus.OK).body(cardDetailsResponse);
	}
	
	@Autowired
	CardDetailService cardDetailService;
	@RequestMapping(value = "/card", consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json", method = RequestMethod.POST)
	/*public Card saveCardData(@RequestParam("cardData") String cardData) throws IOException {
		
		Card chequeDataJson = chequeService.getJson(cardData);
		return chequeDataRepository.save(chequeDataJson);

	}*/
	
	public ResponseEntity<CardDetailsResponse> saveCardData(@RequestBody CardDetail cardDetailJson) {

     
        CardDetailsResponse cardDetailsResponse;
        
     //CardDetail cardDetailJson = cardDetailService.getJson(cardData);
     // CardDetail cardDetailJson = cardData;
      
      System.out.println("name - " + cardDetailJson.getName());
      System.out.println("number - " + cardDetailJson.getCardNumber());
      System.out.println("limit - " + cardDetailJson.getCardLimit());
      System.out.println("bal - " + cardDetailJson.getBalance());
      boolean isValidCard = cardDetailService.validateCreditCardNumber(cardDetailJson.getCardNumber());
        
        if (null != cardDetailJson.getCardNumber() && (null != cardDetailJson.getName() || "" != cardDetailJson.getName()) && isValidCard && cardDetailJson.getBalance()>=0  && cardDetailJson.getCardLimit()>=0){     	
        	
        	cardDetailsDataRepository.save(cardDetailJson);
        	 System.out.println("bal - " + cardDetailJson.getBalance());
        	
        	cardDetailsResponse = new CardDetailsResponse(cardDetailsDataRepository.findAll());
        	 System.out.println("all - " + cardDetailsDataRepository.findAll().size() +"----"+cardDetailsResponse.getCardDetailList().size());
        			//cardDetailsDataRepository.findAll();
            return ResponseEntity.status(HttpStatus.OK).body(cardDetailsResponse);
        } else {
            if (!isValidCard ) {
            	cardDetailsResponse = new CardDetailsResponse("Invalid Card Number.");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(cardDetailsResponse);
            }
            if (null == cardDetailJson.getName() || "" == cardDetailJson.getName()) {
            	cardDetailsResponse = new CardDetailsResponse("Invalid Name.");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(cardDetailsResponse);
            } 
            if (cardDetailJson.getBalance()<0 ) {
            	cardDetailsResponse = new CardDetailsResponse("Invalid Balance.");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(cardDetailsResponse);
            }
            if (cardDetailJson.getCardLimit()<0) {
            	cardDetailsResponse = new CardDetailsResponse("Invalid Card Limit.");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(cardDetailsResponse);
            }else {
            	cardDetailsResponse = new CardDetailsResponse("Internal server error. Please try again after sometime.");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(cardDetailsResponse);
            }

        }
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<CardDetailsResponse> onException(Exception npe) {
        System.out.println("Exception - " + npe.getMessage());
        CardDetailsResponse cardDetailsResponse = new CardDetailsResponse("Internal server error. Please try again after sometime.");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(cardDetailsResponse);

    }

}
